<?php
   $val = $_GET['page'];
   if ($val == "") {
	   header("Location: /?page=Home");
   }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title><?php $val = $_GET['page']; echo($val); ?> - Hingy's (Hing) Workshop. London Freelance Carpenter and Plumber</title>
  <meta name="google-site-verification" content="-ii5deRwIh64MAHL863JlzMPHp3ycFw1q5Pb2glykZo" />
  <meta name="description" content="My name is Hing, I am a Plumber situated in Central London who specialises in interior refurbishment and plumbing. I have been in the field for the past 7 years therefore I have a wide experience in different projects from small bathroom to an entire house. I am very creative in my works, paying very close attention to detail, utilising the skills I learnt to craft many jobs listed in the Services section of the website. Come give me a ring or Facebook me, and we can have a chat or a cup of coffee!">
    <meta name="keywords" content="carpenter, london, hing, hingy, plumber, refurbish, cheap, fast, decoration, lighting">
    <meta name="copyright" content="HingsWorshop - 2010">
    <meta name="author" content="Hing/Dolan Miu">
    <meta name="email" content="dolan_miu@hotmail.com">
    <meta name="Charset" content="UTF-8">
    <meta name="Distribution" content="Global">
    <meta name="Rating" content="General">
  <link href="styles.css" rel="stylesheet" type="text/css">
  <link href="<?php echo($val); ?>-styles.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="js/prototype.js"></script>
  <script type="text/javascript" src="js/scriptaculous.js?load=effects,builder"></script>
  <script type="text/javascript" src="js/lightbox.js"></script>
  </head>

<body>
<div id="container">
  <div id="logo"></div>
  <div id="header"><h1><?php echo($val); ?></h1></div>
  <div id="mainbg">
  	<div id="navigation">
    	<div id="nav_top">
    	  <ul id="navbar">
    	    <li id="home"><a href="/index.php?page=Home">HOME</a></li>
    	    <li id="contact"><a href="/index.php?page=Contact">CONTACT</a></li>
            <li id="contact"><a href="/index.php?page=Services">SERVICES</a></li>
            <li id="portolio"><a href="/index.php?page=Portfolio">PORTFOLIO</a></li>
  	    </ul>
    	</div>
  	</div>
  	<div id="content">
  		<p>
  		  <?php 
			$error = @include($val . '-content.html'); 
		?>
	  </p>

  	</div>
  </div>
  <div id="footer">
  <a href="http://www.facebook.com">
  <div id="facebookicon"></div></a>
  <a href="http://www.twitter.com">
  <div id="twittericon"></div></a>
  	<div id="footer-content">
  		<?php 
			$error = @include("footer.html"); 
		?>
    </div>
  </div>
    
</div>

    
</div>
</body>
</html>